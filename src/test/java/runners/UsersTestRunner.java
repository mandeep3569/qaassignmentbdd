package runners;


import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(monochrome = true,
        plugin = {"pretty","html:target/reports/api-test-report.html"},
        glue = "stepdefinition",
        tags="@api",
        features = "src/test/resources/features/search/UserValidation.feature"

)
public class UsersTestRunner {
}
