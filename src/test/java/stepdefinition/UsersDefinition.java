package stepdefinition;

import api.UsersAPI;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import org.json.JSONException;
import org.junit.Assert;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.ScenarioContext;

import static utils.ScenarioContext.API_RESPONSE;
public class UsersDefinition {
    private final Logger logger = LoggerFactory.getLogger(UsersDefinition.class);

    @Steps
    private UsersAPI usersAPI;

    @Given("The endpoint URI is already configured")
    public void theEndpointIsAlreadyConfigured() {
        SerenityRest.given();
        logger.info("Request invoked:---> ");
    }

    @Then("user set the base path {string} to URI")
    public void SetTheBasePathToURI(String basePath) {
        usersAPI.setEndpoint(basePath);
    }

    @When("user GET a valid userId randomly from resource")
    public void GETARandomValidUserIdFromResource() {
        ScenarioContext.set(API_RESPONSE,this.usersAPI.getResponseRandomUserId());

    }
    @When("user GET a random userId posts from resource")
    public void GETARandomUserPosts() {
        ScenarioContext.set(API_RESPONSE,this.usersAPI.getResponseRandomUserIdWithQueryParams());
    }

    @And("user set the request header {string} as {string}")
    public void iSetTheRequestHeaderAs(String headType, String headValue) {
        this.usersAPI.SetTheRequestHeader(headType,headValue);
    }
    @Then("user POST data in json format")
    public void iPOSTDataInJsonFormat(String requestBody) {
        ScenarioContext.set(API_RESPONSE,this.usersAPI.post(requestBody));
        logger.info("Request posted:---> ");
    }

    @When("user GET the valid endpoint at {string}")
    public void GETInValidUserIdFromResource(String userID) {
        ScenarioContext.set(API_RESPONSE,this.usersAPI.getResponse(userID));

    }

    @Then("The response body should match")
    public void theResponseBodyShouldMatch(String expectedBody) throws JSONException {
        Response actual = ScenarioContext.getApiResponse();
        JSONAssert.assertEquals(actual.asString(),expectedBody, JSONCompareMode.LENIENT);
        logger.info("Response body asserted:---> ");

    }

    @And("user PUT the post {string} with following data")
    public void iPUTThePostWithFollowingData(String userID, String requestBody) {
        ScenarioContext.set(API_RESPONSE,this.usersAPI.put(userID,requestBody));
        logger.info("PUT asserted:---> ");
    }

    @And("user DELETE the valid post {string}")
    public void iDELETETheValidPost(String userID) {
       ScenarioContext.set(API_RESPONSE,this.usersAPI.deleteResponse(userID));
        logger.info("DELETE asserted:---> ");
    }






}
