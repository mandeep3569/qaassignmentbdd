package stepdefinition;



import com.fasterxml.jackson.core.JsonProcessingException;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.apache.http.HttpStatus;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.Is;
import org.junit.Assert;
import utils.ScenarioContext;
import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.Matchers.is;
import api.model.jSONPlaceholderDTO;

public class CommonSteps {



    @Then("the user sees empty product details response body")
    public void theUserSeesEmptyProductDetailsResponseBody() {

        ScenarioContext.getApiResponse().then().assertThat().statusCode(HttpStatus.SC_OK);
        MatcherAssert.assertThat("Response body was not empty!",ScenarioContext.getApiResponse().asString(),is("[]"));

    }

    @Then("user should have the status code {int} displayed")
    public void verifyResponseStatusCode(int expectedStatusCode) {
        ScenarioContext.getApiResponse().then().statusCode(expectedStatusCode);

    }

    @And("user email should be print on console")
    public void printEmail() {
        System.out.println((ScenarioContext.getApiResponse().jsonPath().get("email")).toString());
    }

    @And("each posts of that user should have valid POST ID")
    public void validatePostId() {
        ArrayList<Integer> ids = ScenarioContext.getApiResponse().jsonPath().get("id");
        ids.forEach((n) -> {
            Assert.assertFalse(Integer.signum(n)==-1 || n==0 || n== null);
        });

    }

    @Then("the body response content should be matched")
    public void responseBodyContentMatched(DataTable table) throws JsonProcessingException {

        jSONPlaceholderDTO Body = ScenarioContext.getApiResponse()
                .getBody().as(jSONPlaceholderDTO.class) ;
        List<jSONPlaceholderDTO> actualBody = new ArrayList<>();
        actualBody.add(Body);

        table.asMaps().forEach(key -> {
            Assert.assertTrue("value is not present" + key.toString(),
                    actualBody.stream().anyMatch(p ->
                            p.getUserId() == Integer.parseInt(key.get("userId")) &&
                                    p.getTitle().trim().equalsIgnoreCase(key.get("title").trim()) &&
                                    p.getBody().trim().equalsIgnoreCase(key.get("body").trim())

                    ));
        });


    }




}
