@user @api
Feature: Validate User details from the API - (https://jsonplaceholder.typicode.com/users/1)

  Background: Endpoint Configuration
    Given The endpoint URI is already configured

  @user @positive
  Scenario: Enter a valid userId and verify email
    Then user set the base path "/users" to URI
    When user GET a valid userId randomly from resource
    Then user should have the status code 200 displayed
    And user email should be print on console

  @posts @positive
  Scenario: Verify the random user posts
    Then user set the base path "/posts" to URI
    When user GET a random userId posts from resource
    Then user should have the status code 200 displayed
    And each posts of that user should have valid POST ID

  @post
  Scenario: User create post using POST
    Then user set the base path "/posts" to URI
    And user set the request header "Content-Type" as "application/json"
    Then user POST data in json format
    """
     {
      "userId": 12,
      "id": 101,
      "title": "New title",
      "body": "New Body"
    }
    """
    Then  user should have the status code 201 displayed
    And the body response content should be matched
      | userId | title     | body     |
      | 12     | New title | New Body |

  @invalid
  Scenario: User get invalid Post
    Then user set the base path "/posts" to URI
    When user GET the valid endpoint at "155"
    Then user should have the status code 404 displayed

  @posts @positive
  Scenario: User get the Valid Posts
    Then user set the base path "/posts" to URI
    When user GET the valid endpoint at "1"
    Then user should have the status code 200 displayed
    Then The response body should match
  """
  {
  "userId": 1,
  "id": 1,
  "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
  "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
 }
  """

  @put
  Scenario: User UPDATE the post
    Then user set the base path "/posts" to URI
    And user set the request header "Content-Type" as "application/json"
    And user PUT the post "1" with following data
      """
      {
        "id": 1,
        "name": "Leanne Graham111",
        "username": "Bret111",
        "email": "111Sincere@april.biz"
      }
     """
    Then user should have the status code 200 displayed


  @delete
  Scenario: User DELETE the Post
    Then user set the base path "/posts" to URI
    And user DELETE the valid post "1"
    Then user should have the status code 200 displayed