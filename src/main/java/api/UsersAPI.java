package api;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Random;

public class UsersAPI extends BaseAPI{

    private final Logger logger = LoggerFactory.getLogger(UsersAPI.class);

    private String BASE_PATH;
    Random random = new Random();
    private final Integer RANDOM_USER_ID = random.nextInt(10-1+1)+1;

    public void setEndpoint(String basePath){
        BASE_PATH=basePath;
        logger.info("Base Path set: " + BASE_PATH);

    }

    public Response getResponse(String endP) {
        return getRequestSpecification()
                .basePath(BASE_PATH).get("/" + endP);

    }

    public Response getResponseRandomUserId() {
        return getRequestSpecification()
                .basePath(BASE_PATH).get("/" + RANDOM_USER_ID);

    }

    public Response getResponseRandomUserIdWithQueryParams() {
        return getRequestSpecification()
                .basePath(BASE_PATH).queryParams("userId",RANDOM_USER_ID).get();

    }

    public Response post(String requestBody) {
        return getRequestSpecification()
                .body(requestBody).basePath(BASE_PATH).contentType(ContentType.JSON).log().all().post();

    }
    public void SetTheRequestHeader(String headType, String headValue) {
        getRequestSpecification().header(headType, headValue);
        logger.info("Response header set:---> ");
    }

    public Response put(String userID, String requestBody) {
        return getRequestSpecification()
                .body(requestBody).basePath(BASE_PATH).put("/" + userID);

    }

    public Response deleteResponse(String endP) {
        return getRequestSpecification()
                .basePath(BASE_PATH).delete("/" + endP);

    }

}
