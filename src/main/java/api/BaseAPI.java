package api;

import env.ApplicationProperties;
import env.Environment;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;

public abstract class BaseAPI {

    ApplicationProperties appProps = Environment.INSTANCE.getApplicationProperties();

    /**
     *  common specification for request
     */

    protected RequestSpecification getRequestSpecification() {
        RequestSpecification rSpec = SerenityRest.given().contentType(ContentType.JSON).baseUri(appProps.getBaseURL());
//        rSpec.contentType(ContentType.JSON).baseUri(appProps.getBaseURL());
        System.out.println("BaseURL is _______ : "+ appProps.getBaseURL());
        return rSpec;
    }


}
